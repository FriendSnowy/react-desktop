 <?php
    $method = $_GET['method'];
    $returnValue = "";

    function getCityByIp() {
        $ip = $_SERVER['REMOTE_ADDR'];
        if ($ip == '127.0.0.1') $ip = '195.64.208.218';
        $city = json_decode(
            file_get_contents(
                'https://ipinfo.io/' . $ip . '/json'
            ), true
        );
        return $city['city'];
    }

    function getWeatherText($actualWeather) {
        $weather = "";
        if ($actualWeather >= 200 && $actualWeather < 300)
            $weather = "Гроза";
        else if ($actualWeather >= 300 && $actualWeather < 400)
            $weather = "Мелкий дождь";
        else if ($actualWeather >= 500 && $actualWeather < 600)
            $weather = "Дождь";
        else if ($actualWeather >= 600 && $actualWeather < 700)
            $weather = "Снег";
        else if ($actualWeather >= 700 && $actualWeather < 800)
            $weather = "Туман";
        else if ($actualWeather == 800)
            $weather = "Ясно";
        else if ($actualWeather > 800 && $actualWeather < 900)
            $weather = "Облачно";
        return $weather;
    }

    if (!empty($method)) {

        if ($method == "rss") {
            if (!empty($_GET['rssURL'])) {
                if (empty($_GET['count']) || $_GET['count'] == 0) {
                    $data = json_decode(
                        file_get_contents(
                            'https://api.rss2json.com/v1/api.json?rss_url=' . urlencode($_GET['rssURL']) . '&api_key=xxi06tthzjp7anxpbouslfyury3xabcmnrxn0u3v'
                        ), true
                    );

                    $returnValue = json_encode($data['items'], JSON_UNESCAPED_UNICODE);
                } else {
                    $data = json_decode(
                        file_get_contents(
                            'https://api.rss2json.com/v1/api.json?rss_url=' . urlencode($_GET['rssURL']) . '&api_key=xxi06tthzjp7anxpbouslfyury3xabcmnrxn0u3v'
                        ), true
                    );

                    $data = $data['items'];
                    $data = array_slice($data, 0, $_GET['count']);
                    $returnValue = json_encode($data, JSON_UNESCAPED_UNICODE);
                }
            }
            else {
                $returnValue = json_encode("error");
            }
        }

        else if ($method == "weather") {
            $key = '84101ffff79f76a7087e6c4883728650';
            $baseURL = 'http://api.openweathermap.org/data/2.5/';
            $data = '';

            if (empty($_GET['type']) || $_GET['type'] == "actual") {
                if (!(empty($_GET['lon']) && empty($_GET['lat']))) {
                    $data = json_decode(
                        file_get_contents(
                            $baseURL . 'weather?lat=' . $_GET['lat'] . '&lon=' . $_GET['lon'] . '&appid=' . $key
                        ), true
                    );
                } else {
                    $city = getCityByIp();
                    $data = json_decode(
                        file_get_contents(
                            $baseURL . 'weather?q=' . $city . '&appid=' . $key
                        ), true
                    );
                }

                $actualWeather = $data['weather'][0]['id'];
                $data['weather'][0]['main'] = getWeatherText($actualWeather);
                $data['main']['temp'] = $data['main']['temp'] - 273.15;

                $returnValue = json_encode($data, JSON_UNESCAPED_UNICODE);
            }

            else if ($_GET['type'] == "forecast") {
                $city = getCityByIp();
                $data = json_decode(
                    file_get_contents(
                        $baseURL . 'forecast?q=' . $city . '&appid=' . $key
                    ), true
                );

                $output = null;

                $k = 0;
                $timeType = 0;
                for ($i = 1; $i < count($data['list']); $i+=2) {
                    $actualList = $data['list'][$i];
                    $time = "night";
                    switch ($timeType) {
                        case 0:
                            $time = "night";
                            break;
                        case 1:
                            $time = "morning";
                            break;
                        case 2:
                            $time = "day";
                            break;
                        case 3:
                            $time = "evening";
                            break;
                        default:
                            break;
                    }
                    $output[$k][$time]['temp'] = $actualList["main"]["temp"] - 273.15;
                    $output[$k][$time]['weather'] = getWeatherText($actualList["weather"][0]["id"]);
                    $output[$k][$time]['wind'] = $actualList["wind"]["speed"];
                    $output[$k][$time]['date'] = $actualList["dt_txt"];
                    if ($timeType == 3) {
                        $k++;
                        $timeType = 0;
                    }
                    else {
                        $timeType++;
                    }
                }

                $returnValue = json_encode($output, JSON_UNESCAPED_UNICODE);
            }
        }
    }

    else {
        $returnValue = "No method in api request";
    }

    echo $returnValue;
 ?>