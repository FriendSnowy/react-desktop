const dataTypes = {
    news : 0,
    weather: 1
};

const newsOutTypes = {
    title : 0,
    description: 1
};

var intervals = {
    intervals:{},
    make : function(name, action, timeout) {
        let id = setInterval(action, timeout);
        if (this.exist(name)) name += "_copy";
        intervals[name] = id;
    },
    clear : function(name) {
        clearInterval(intervals[name]);
    },
    exist : function(name) {
        return (intervals[name] != null && intervals[name] != undefined)
    }
};

const Factories = {
    WidgetFactory : {
        getWidget : function (type, context, data) {
            return (
                <Widget
                    type = {type}
                    params = {data}
                    functions = {{
                        changeRSSUrl : context.setInfoRSSUrl,
                        changeInfoColor : context.setInfoColor,
                        showInfo : context.toggleInfoBlockState,
                        changeInfoType : context.setInfoOutType,
                        changeCalled : context.setCaller
                    }}
                />
            );
        },
        newWidget : function(type, context, data) {
            let out = undefined;
            if (type == dataTypes.news) {
                if (data.name != undefined && data.color != undefined && data.rssURL != undefined && data.outType != undefined) {
                    out = this.getWidget(type, context, data);
                }
                else
                    out = null;
            }
            else if (type == dataTypes.weather) {
                if (data.name != undefined && data.color != undefined) {
                    out = this.getWidget(type, context, data);
                }
                else
                    out = null;
            }

            if (out != null)
                return out;
            else throw new SyntaxError("Wrong params for widget type " + type );
        }
    },

    ContentFactory : {
        newContent : function(type, data) {
            switch (type) {
                case dataTypes.news:
                    if (data.rssURL != undefined && data.outType != null) {
                        return (
                            <NewsWidgetContent
                                reqURL={data.rssURL}
                                type={data.outType}
                            />
                        );
                    }
                    else throw new SyntaxError("Wrong params for content type " + type );
                    break;
                case dataTypes.weather:
                    return (
                        <WeatherWidgetContent />
                    );
            }
        }
    }
};

//------------------------------------- ВИДЖЕТЫ ------------------------------------------------------------

class Widget extends React.Component {
    constructor (props) {
        super (props);
        this.showInfo = this.showInfo.bind(this);
    }

    showInfo() {
        this.props.functions.changeRSSUrl(this.props.params.rssURL);
        this.props.functions.changeInfoColor(this.props.params.color);
        this.props.functions.changeInfoType(this.props.type);
        this.props.functions.changeCalled(this.props.params.name);
        this.props.functions.showInfo(true);
    }

    render() {
        let content = Factories.ContentFactory.newContent(this.props.type, this.props.params);

        return (
            <div className="object">
                <div className="title" style = {{backgroundColor: this.props.params.color}} onClick = {this.showInfo}>
                    <h2>{this.props.params.name}</h2>
                </div>
                {content}
            </div>
        )
    }
}

class NewsWidgetContent extends Widget { //Контент виджета - новости
    constructor(props){
        super(props);
        this.state = {
            data: undefined
        };
        this.update = this.update.bind(this);
    }

    update() {
        let _this = this;
        let method = "rss&count=1&rssURL=" + this.props.reqURL;
        apiAsyncRequest(method, function(resp) {
            _this.setState({
                data: JSON.parse(resp)
            })
        })
    }

    componentWillMount(){
        this.update();
    }

    componentDidMount() {
        let _this = this;
        intervals.make("newsInterval", _this.update, 60000);
    }

    render() {
        if (this.state.data == undefined) {
            return (
                <div className="content-wrapper">
                    <div className="desc news">
                        <p>Loading</p>
                    </div>
                </div>
            )
        }
        else if (this.state.data == "error") {
            return (
                <div className="content-wrapper">
                    <div className="desc news">
                        <p className = "error">При загрузке данных с rss возникла ошибка</p>
                    </div>
                </div>
            )
        }
        else if (this.state.data != undefined && this.state.data != "e") {
            let widgetContent = null;
            if (this.props.type == newsOutTypes.title)
                widgetContent = this.state.data[0].title.replace(/<\/?[^>]+>/g,'');
            else
                widgetContent = this.state.data[0].description.replace(/<\/?[^>]+>/g,'');
            if (widgetContent.length > 85)
                widgetContent = widgetContent.substr(0, 85) + "...";
            return (
                <div className="content-wrapper">
                    <div className="desc news">
                        <p>{widgetContent}</p>
                    </div>
                    <a href={this.state.data[0].link} className="more" target = "_blank">Подробнее</a>
                </div>
            )
        }
        else throw new Error ("Неизвестная ошибка");
    }
}

class WeatherWidgetContent extends React.Component { //Контент виджета - погоды
    constructor(props){
        super(props);
        this.update = this.update.bind(this);
        this.state = {
            data: undefined
        }
    }

    update() {
        let _this = this;
        apiAsyncRequest("weather", function(resp){
            _this.setState({
                data: JSON.parse(resp)
            })
        })
    }

    componentWillMount(){
        this.update();
    }

    componentDidMount() {
        let _this = this;
        intervals.make("newsInterval", _this.update, 60000);
    }

    render() {
        return (
            <div className="content-wrapper">
                <div className="desc weather">
                    <p>За окном: {this.state.data == undefined ? "" : this.state.data.weather[0].main}</p>
                    <p>Температура воздуха: {this.state.data == undefined ? "" : this.state.data.main.temp} °C</p>
                    <p>Ветер: {this.state.data == undefined ? "" : this.state.data.wind.speed} м/с</p>
                </div>
            </div>
        )
    }
}

//------------------------------------- КОНЕЦ ВИДЖЕТОВ -----------------------------------------------------

//------------------------------------      ПОП-АП     -----------------------------------------------------

class InfoBlock extends React.Component{
    constructor(props) {
        super(props);
        this.hideInfo = this.hideInfo.bind(this);
    }

    hideInfo() {
        this.props.showInfo(false);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.calledFrom != this.props.calledFrom)
            this.refs.content.update();
    }
    render() {
        let output = null;
        switch (this.props.infoType) {
            case dataTypes.news:
                output =
                    <InfoNewsContent
                        ref = "content"
                        color = {this.props.color}
                        rssURL = {this.props.rssURL}
                        calledFrom = {this.props.calledFrom}
                    />;
                break;
            case dataTypes.weather:
                output =
                    <InfoWeatherContent
                        ref = "content"
                        color = {this.props.color}
                        calledFrom = {this.props.calledFrom}
                    />;
                break;
            default:
                break;
        }
        return (
            <div className={"popUp" + (this.props.visible ? "" : " hidden")}>
                <div className="popUpController" onClick = {this.hideInfo}></div>
                {output}
            </div>
        )
    }
}

class InfoNewsContent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data: undefined
        };
        this.update = this.update.bind(this);
    }

    update() {
        let _this = this;
        let method = "rss&rssURL=" + this.props.rssURL;
        apiAsyncRequest(method, function (resp) {
            _this.setState({
                data: JSON.parse(resp)
            });
        });
    }

    componentDidMount() {
        if (intervals.exist("infoInterval"))
            intervals.clear("infoInterval");
        intervals.make("infoInterval", this.update, 60000);
    }

    render() {
        let output = null;
        if (this.state.data == undefined)
            output = "Loading";
        else if (this.state.data == "error")
            output = <p className = "error">Ошибка при получении данных по rss</p>;
        else if (this.state.data != undefined && this.state.data != "error") {
            output = this.state.data.map(
                (item,index)=><SingleNews info = {item} key = {index} color = {this.props.color}/>
            )
        }
        else
            output = <p className = "error">Неизвестная ошибка</p>;
        return (
            <div className="popUpContent">
                {output}
            </div>
        )
    }
}

class SingleNews extends React.Component{
    render() {
        let title = this.props.info.title;
        let content = this.props.info.content;
        let link = this.props.info.link; 
        let date = this.props.info.pubDate.substr(5).replace('-', '.');
        date = date.substr(0, date.length - 3);
        return (
            <div className="singleNews" style = {{borderColor: this.props.color}}>
                <h2 className="title">{title}</h2>
                <p className="date">{date}</p>
                <p className="content">{content.replace(/<\/?[^>]+>/g,'').replace(/Читать дальше →/g, '')}</p>
                <a href={link} className="more" target = "_blank">Подробнее</a>
            </div>
        )
    }
}

class InfoWeatherContent extends React.Component {
    constructor (props) {
        super (props);
        this.update = this.update.bind(this);
        this.state = {
            data: undefined
        }
    }

    update() {
        let _this = this;
        let method = "weather&type=forecast";
        apiAsyncRequest (method, function(resp) {
            _this.setState({
                data: JSON.parse(resp)
            });
        });
    }
    componentDidMount() {
        if (intervals.exist("infoInterval"))
            intervals.clear("infoInterval");
        intervals.make("infoInterval", this.update, 60000);
    }

    render() {
        let output = null;
        if (this.state.data == undefined)
            output = "Loading";
        else if (this.state.data == "error")
            output = <p className = "error">Ошибка при получении данных по rss</p>;
        else if (this.state.data != undefined && this.state.data != "error") {
            output = this.state.data.map(
                (item,index)=><SingleWeather info = {item} key = {index} color = {this.props.color}/>
            )
        }
        else
            output = <p className = "error">Неизвестная ошибка</p>;
        return (
            <div className="popUpContent">
                {output}
            </div>
        )
    }
}

class SingleWeather extends React.Component {
    render() {
        return (
            <p>Test</p>
        )
    }
}

//------------------------------------  КОНЕЦ ПОП-АПА  -----------------------------------------------------
class App extends React.Component{
    constructor(props){
        super(props);
        this.toggleInfoBlockState = this.toggleInfoBlockState.bind(this);
        this.setInfoRSSUrl = this.setInfoRSSUrl.bind(this);
        this.setInfoColor = this.setInfoColor.bind(this);
        this.setInfoOutType = this.setInfoOutType.bind(this);
        this.setCaller = this.setCaller.bind(this);
        this.state = {
            showInfoBlock: false,
            infoRSSUrl: undefined,
            infoInfoType : undefined,
            borderColor: "black",
            calledFrom: null
        };
    }

    toggleInfoBlockState(visible) {
        this.setState({
            showInfoBlock: visible
        })
    }

    setInfoOutType(type) {
        this.setState({
            infoInfoType: type
        })
    }

    setInfoRSSUrl(url) {
        this.setState({
            infoRSSUrl: url
        })
    }

    setInfoColor(color){
        this.setState({
            borderColor: color
        })
    }

    setCaller(caller) {
        this.setState ({
            calledFrom: caller
        })
    }

    render() {
        let that = this;
        return (
            <div className = "wrapper">
                <InfoBlock
                    color = {this.state.borderColor}
                    visible = {this.state.showInfoBlock}
                    showInfo = {this.toggleInfoBlockState}
                    infoType = {this.state.infoInfoType}
                    rssURL = {this.state.infoRSSUrl}
                    calledFrom = {this.state.calledFrom}
                />
                <div className="object-wrapper">
                    <div className="column">
                        {Factories.WidgetFactory.newWidget(dataTypes.weather, that, {
                            name : 'Погода',
                            color : 'green'
                        })}
                        {Factories.WidgetFactory.newWidget(dataTypes.news, that, {
                            name : 'Новости',
                            color: '#b88b58',
                            rssURL : 'https://meduza.io/rss/all',
                            outType : newsOutTypes.title
                        })}
                        {Factories.WidgetFactory.newWidget(dataTypes.news, that, {
                            name : 'Хабрахабр',
                            color: '#558FAA',
                            rssURL : 'https://habrahabr.ru/rss/interesting/',
                            outType : newsOutTypes.title
                        })}
                        {Factories.WidgetFactory.newWidget(dataTypes.news, that, {
                            name : 'Баш',
                            color: '#A3A3A3',
                            rssURL : 'http://bash.im/rss/',
                            outType : newsOutTypes.description
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);