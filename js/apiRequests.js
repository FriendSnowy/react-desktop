function apiAsyncRequest (method, callback) {
    var req = new XMLHttpRequest();
    var reqURL = "/api.php?method=" + method;
    console.log(reqURL);
    req.open("GET", reqURL, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.send();

    req.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (typeof callback == "function") {
                callback(this.responseText);
            }
            else
                throw new Error("callBack is not a function")
        }
    }
}